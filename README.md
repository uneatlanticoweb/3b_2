## Servidor en NodeJS que devuelve contenido de texto al cliente

El servidor devuelve al cliente el contenido de la carpeta **dist**. Para instalarlo seguir los siguientes pasos:

1. `$git clone https://uneatlanticoweb@bitbucket.org/uneatlanticoweb/3b_2.git`
2. `$node index.js`
3. Visitar http://localhost:8080/ o http://localhost:8080/texto.txt o http://localhost:8080/texto_largo.txt