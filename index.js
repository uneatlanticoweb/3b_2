var http = require('http');
var fs = require('fs');

var server = http.createServer(function (req, res) {
    var chunkNro = 0;
    var ruta = (req.url == "/")? "/index.html" : req.url; 
    res.writeHead(200, { 'Content-Type': 'text/html' });
    
    var stream = fs.createReadStream("dist"+ruta, {encoding: 'utf8'});
    stream.on('data', (chunk) => {
        chunkNro++;
        res.write(chunk);
    });
    stream.on('end', () => {
        console.log("Nro de Chunks :"+chunkNro);
        res.end();
    });
    stream.on('error', (error) => {
        console.error("Aquí hubo un error "+error);
        res.writeHead(404, { 'Content-Type': 'text/html' });
        res.write("Recurso dist"+ruta+" no encontrado ")
        res.end();
    });
});

server.listen(8080);
console.log("Escuchando en http://localhost:8080/");